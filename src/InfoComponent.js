import React from 'react';

function InfoComponent() {
  return (
    <>
      <div>Прізвище, ім'я: Кушнарьов Олексій</div>
      <div>Спеціалізація: фронтенд</div>
      <div>Сертифікація: Javascript certification від International JavaScript Institute</div>
      <div>Пояснення: я обрав саме цю сертифікацію від International JavaScript Institute через їхню репутацію як авторитетної організації, що забезпечує високий стандарт навчання JavaScript, а також через те, що ця сертифікація допоможе мені довести свою експертність у JavaScript і підвищити свої шанси на успіх в області програмування та розробки веб-сайтів та додатків.</div>
    </>
  );
}

export default InfoComponent;