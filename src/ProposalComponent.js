import React from 'react';
import {Temporal, Intl, toTemporalInstant} from '@js-temporal/polyfill';

Date.prototype.toTemporalInstant = toTemporalInstant;

function ProposalComponent() {
    const date = Temporal.PlainDate.from({year: 2006, month: 8, day: 24}); // => 2006-08-24
    const timeZone = Temporal.TimeZone.from('Africa/Cairo');
    return (
        <>
            {date.year}<br/>
            {date.toString()}<br/>
            {String(timeZone.getInstantFor('2000-01-01T00:00'))}<br/>
            {String(timeZone.getPlainDateTimeFor('2000-01-01T00:00Z'))}<br/>
            {String(timeZone.getPreviousTransition(Temporal.Now.instant()))}<br/>
            {String(timeZone.getPreviousTransition(Temporal.Now.instant()))}<br/>
            {String(timeZone.getNextTransition(Temporal.Now.instant()))}
        </>
    );
}

export default ProposalComponent;