import logo from './logo.svg';
import './App.css';
import InfoComponent from "./InfoComponent";
import ProposalComponent from "./ProposalComponent";

function App() {
  return (
    <div className="App">
      <InfoComponent />
        <br/>
        <ProposalComponent />
    </div>
  );
}

export default App;
